#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import sys
import socketserver



class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """




    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        Servidor = str(self.client_address[0])
        Puerto = str(self.client_address[1])

        for line in self.rfile:
            print("El cliente nos manda ", line.decode('utf-8'))

        self.wfile.write('Servidor: ' + Servidor + '\n' + 'Puerto: ' + Puerto)

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    Port = int(sys.argv[1])
    serv = socketserver.UDPServer(('', Port), EchoHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
